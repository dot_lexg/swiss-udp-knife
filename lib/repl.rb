# frozen_string_literal: true

require 'reline'
require 'reline/puts'
require 'colorize'

class Repl
	def initialize(knife)
		@knife = knife
	end

	def run
		Thread.new do
			loop do
				do_recvfrom
			end
		end

		while (command_line = Reline.readline("#{@knife.inspect}> ", true))
			do_command(command_line)
		end
	end

	def do_recvfrom
		message, from = *@knife.recvfrom(1 << 16)
		_, port, _, host = from
		Reline.puts "recvfrom #{host} #{port}: #{message.inspect}".green

		# TODO: add commandline flag and/or command for this
		# unless message.start_with?(':: ') || @knife.connected?
		# 	Reline.puts "  autoreplying...".green
		# 	@knife.send(":: I received #{message.inspect} from you.", 0, host, port)
		# end
	rescue SystemCallError => e
		puts "recvfrom #{e}".yellow
	end

	def do_command(command_line)
		command, args = command_line.split(' ', 2)
		if COMMANDS.include?(command)
			public_send(command, args)
		else
			puts "Invalid command, type 'help' for help"
		end
	rescue ArgumentError => e
		puts "#{command}: Invalid arguments: #{e}"
	rescue SystemCallError => e
		puts e.to_s
	end

	COMMANDS = %w[help bind connect send sendto stun].freeze

	def help(args)
		puts 'Ignoring extra parameters' if args
		puts 'Commands:'
		COMMANDS.each {|x| puts "    #{x}" }
	end

	def bind(args)
		args = args.split
		case args.length
		when 1
			@knife.bind('0.0.0.0', Integer(args[0]))
		when 2
			host, port = args
			@knife.bind(host, Integer(port))
		else
			puts "Usage: #{__method__} [<source_ip>] <port>"
		end
	end

	def connect(args)
		args = args.split
		if args.length == 2
			host, port = args
			@knife.connect(host, Integer(port))
		else
			puts "Usage: #{__method__} <host> <port>"
		end
	end

	def send(message)
		@knife.send(message, 0)
	end

	def sendto(args)
		args = args.split(' ', 3)
		if args.length == 3
			host, port, message = args
			@knife.send(message, 0, host, Integer(port))
		else
			puts "Usage: #{__method__} <host> <port> <message>"
		end
	end

	def stun(args)
		args = args.split
		if args.length == 2
			host, port = args
			external_addr, external_port = @knife.stun_binding_request(host, port)
			puts "external: #{external_addr} #{external_port}"
		else
			puts "Usage: #{__method__} <host> <port>"
		end
	end
end
