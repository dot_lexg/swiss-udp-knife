# frozen_string_literal: true

require 'securerandom'
require 'socket'
require 'ipaddr'

module Stun
	# Protocol constants
	STUN_COOKIE = 0x2112A442
	AT_MAPPED_ADDR = 0x0001
	AT_XOR_MAPPED_ADDR = 0x0020
	MAPPED_ADDR_IPv4 = 0x01
	MAPPED_ADDR_IPv6 = 0x02
	MTU = 66534

	# Request parameters
	RQ_TYPE = 0x0001
	RQ_LEN = 0 # Not sending any attributes

	# Expected response parameters
	R_TYPE = 0x0101

	class StunError < IOError; end

	class << self
		def binding_request(socket, stun_host, stun_port, max_timeout: 5)
			request_id = SecureRandom.random_bytes(12)
			request = [RQ_TYPE, RQ_LEN, STUN_COOKIE, request_id].pack('nnNa12')
			response_ready = false
			timeout = 0.25

			loop do
				begin
					socket.send(request, 0, stun_host, stun_port) unless response_ready
					return parse_result(request_id, socket.recv_nonblock(MTU))
				rescue IO::WaitReadable
					raise StunError, 'NAT punchthrough timed out' if timeout >= max_timeout

					response_ready = !IO.select([socket], [], [], timeout).nil?
				end
				timeout *= 2
			end
		end

		private

		def parse_result(request_id, response)
			raise StunError, 'response too short to be a STUN packet' if response.length < 20

			r_type, r_len, r_cookie, r_id = response.slice!(0...20).unpack('nnNa12')
			raise StunError, 'response was not a STUN packet' if r_cookie != STUN_COOKIE
			raise StunError, 'response msg_type is 0x%04X, expected 0x%04X' % [r_type, R_TYPE] if r_type != R_TYPE
			raise StunError, 'response did not have the correct id' if r_id != request_id
			raise StunError, "response claims length of #{r_len} bytes, actually #{response.length} bytes" if r_len != response.length

			extract_addrinfo(request_id, response)
		end

		def extract_addrinfo(request_id, response_body)
			until response_body.empty?
				raise StunError, 'response contains incomplete attribute header' if response_body.length < 4

				at_type, at_len = response_body.slice!(0...4).unpack('nn')
				raise StunError, 'response contains incomplete attribute' if response_body.length < at_len

				at_body = response_body.slice!(0...at_len)

				case at_type
				when AT_MAPPED_ADDR, AT_XOR_MAPPED_ADDR
					result = parse_mapped_address(request_id, at_body, xor: at_type == AT_XOR_MAPPED_ADDR)
					return result if result
				end
			end

			nil
		end

		def parse_mapped_address(request_id, at_body, xor:)
			raise StunError, 'response contains malformed MAPPED-ADDRESS attribute' if at_body.length < 8

			addr_proto, addr_stun_port = at_body.unpack('xCn')
			addr_ip_ary = unpack_ip_ary(at_body, addr_proto)

			if xor
				addr_stun_port ^= STUN_COOKIE >> 16
				addr_ip_ary[0] ^= STUN_COOKIE
				if addr_proto == MAPPED_ADDR_IPv6
					(0...3).each {|i| addr_ip_ary[i + 1] ^= request_id[i * 4, 4].unpack1('N') }
				end
			end

			[IPAddr.new_ntoh(addr_ip_ary.pack('N*')).to_s, addr_stun_port]
		end

		def unpack_ip_ary(at_body, addr_proto)
			case addr_proto
			when MAPPED_ADDR_IPv4
				raise StunError, 'response contains malformed MAPPED-ADDRESS attribute' if at_body.length != 8

				at_body.unpack('@4N')
			when MAPPED_ADDR_IPv6
				raise StunError, 'response contains malformed MAPPED-ADDRESS attribute' if at_body.length != 20

				at_body.unpack('@4N4')
			else
				raise StunError, "MAPPED-ADDRESS protocol #{addr_proto} refers to unsupported IP family"
			end
		end
	end
end
