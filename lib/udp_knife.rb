# frozen_string_literal: true

require 'socket'
require_relative 'repl'
require_relative 'stun'

class UDPKnife
	def initialize
		@sock = UDPSocket.new
		@sock.do_not_reverse_lookup = true
		@bound = false
		@connected = false
	end

	def bound? = @sock.addr[1] != 0

	def connected? = @connected

	def bind(host, port) = @sock.bind(host, port)

	def connect(host, port)
		@sock.connect(host, port)
		@connected = true
	end

	def recvfrom(mtu) = @sock.recvfrom(mtu)

	def send(message, flags, *to) = @sock.send(message, flags, *to)

	def stun_binding_request(host, port) = Stun.binding_request(@sock, host, port)

	def repl = Repl.new(self).run

	def inspect
		bind_str = bound? ? inspect_addr(@sock.addr) : 'unbound'
		peer_str = connected? ? inspect_addr(@sock.peeraddr) : 'unconnected'
		"UDP[#{bind_str} -> #{peer_str}]"
	end

	private

	def inspect_addr((_, port, _, host)) = "#{host} #{port}"
end
