#!/usr/bin/env ruby
# frozen_string_literal: true

require 'bundler/setup'
require 'optparse'
require_relative '../lib/udp_knife'

USAGE = "Usage: #{$0} [[-s IP] -b PORT] [<connect_host> <connect_port>]"

bind_ip = nil
bind_port = nil
connect = nil
OptionParser.new do |opts|
	opts.banner = USAGE
	opts.on('-s IP', '--source-ip=IP', 'Specifies the IP to bind to.') do |value|
		bind_ip = value
	end

	opts.on('-b PORT', '--bind=PORT', 'Specifies which port to bind to.') do |value|
		bind_port = Integer(value)
	end
end.parse!

if ARGV.length == 2
	connect = [ARGV[0], Integer(ARGV[1])]
elsif !ARGV.empty?
	warn USAGE
	exit 1
end

knife = UDPKnife.new
if bind_port
	knife.bind(bind_ip || '0.0.0.0', bind_port)
elsif bind_ip
	warn "#{$0}: It is an error to use -s/--source-ip without -b/--bind (Use --bind=0 to bind to an unspecified port.)"
	exit 1
end

knife.connect(*connect) if connect

begin
	knife.repl
rescue Interrupt
	exit 130
end
